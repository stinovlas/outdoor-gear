#!/usr/bin/env python3

import os
from datetime import datetime

AUTHOR = 'Honza “Stínovlas” Musílek'
SITENAME = 'Outdoor gear reviews'
SITEURL = ''

SITEURL = '/'
PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Prague'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Enable translations
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}
PLUGIN_PATHS = [
    os.path.abspath(os.path.join(os.curdir, 'pelican-plugins')),
]
PLUGINS = [
    'i18n_subsites',
]

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (
    ('gitlab', 'http://gitlab.com/stinovlas'),
    ('envelope-o', 'mailto:stinovlas@gmail.com'),
)

DEFAULT_PAGINATION = 5

STATIC_PATHS = [
    'images',
    'extra',
]

# Theme settings
THEME = os.path.abspath(os.path.join(os.curdir, 'Flex'))
MAIN_MENU = True
SITETITLE = 'Outdoor gear reviews'
SITESUBTITLE = AUTHOR
SITELOGO = '/images/avatar.png'
SUMMARY_MAX_LENGTH = 50
COPYRIGHT_NAME = AUTHOR
COPYRIGHT_YEAR = datetime.now().year

EXTRA_PATH_METADATA = {
    'extra/custom.css': {'path': 'static/custom.css'},
}
CUSTOM_CSS = 'static/custom.css'

RELATIVE_URLS = True
